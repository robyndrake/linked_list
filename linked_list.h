#include "lexer_datatype.h"

#ifndef ROBYN_LINKED_LIST
#define ROBYN_LINKED_LIST

typedef enum _datatype {
	TYPE,
	IDENTIFIER,
	OPERAND,
	OPERATOR,
	BRACE_OPEN,
	BRACE_CLOSE
}DataType;

typedef struct _data {
	DataType type;
	union {
		Type,
		Identifer,
		Operand,
		Operator
	}
}Data;

typedef struct _list {
	struct _list *prev;
	struct _list *next;
	void *data;
}List;

List head, tail;

#endif
